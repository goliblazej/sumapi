<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate sum object
include_once '../objects/sum.php';
 
$database = new Database();
$db = $database->getConnection();
 
$sum = Sum::constructWithDb($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

if((int)$data->first_summand > 0 && 
   (int)$data->first_summand < 50 &&
   (int)$data->second_summand > 0 &&
   (int)$data->second_summand < 50) {
  // set sum property values
  $sum->first_summand = $data->first_summand;
  $sum->second_summand = $data->second_summand;
  $sum->result = $data->result;
  
  $sum->create();
}
else{
  print_r(json_encode("Error: Values need to be in range [1-50]"));
}
?>