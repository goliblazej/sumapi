<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include  object file
include_once '../objects/sum.php';
 
// initialize object
$sum = new Sum();
 
// set summand properties of sum
$sum->first_summand = isset($_GET['first_summand']) ? $_GET['first_summand'] : die();
$sum->second_summand = isset($_GET['second_summand']) ? $_GET['second_summand'] : die();

if((int)$sum->first_summand > 0 && 
   (int)$sum->first_summand < 50 &&
   (int)$sum->second_summand > 0 &&
   (int)$sum->second_summand < 50) {
    // calculate sum
    $sum->result = $sum->getSum($sum->first_summand, $sum->second_summand);
 
   // make it json format
    print_r(json_encode($sum->result));
}
else{
  print_r(json_encode("Error: Values need to be in range [1-50]"));
}

?>