<?php
class Sum{

  // database connection and table name
  private $conn;
  private $table_name = "sums";

  // object properties
  public $first_summand;
  public $second_summand;
  public $result;

  // constructor without database connection
  public function __construct(){
  }

  // constructor with $db as database connection
  public static function constructWithDb($db) {
    $obj = new Sum(); 
    $obj->conn = $db;
    return $obj;
  }

  // create sum
  function create(){
  
    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
            first_summand=:first_summand, second_summand=:second_summand, result=:result";
    
    // prepare query
    $stmt = $this->conn->prepare($query);
    
    // sanitize
    $this->first_summand=htmlspecialchars(strip_tags($this->first_summand));
    $this->second_summand=htmlspecialchars(strip_tags($this->second_summand));
    $this->result=htmlspecialchars(strip_tags($this->result));
    
    // bind values
    $stmt->bindParam(":first_summand", $this->first_summand);
    $stmt->bindParam(":second_summand", $this->second_summand);
    $stmt->bindParam(":result", $this->result);
    
    // execute query
    if($stmt->execute()){
        return true;
    }
    return false;
  }

  function getSum($first_summand, $second_summand){
    return $first_summand+$second_summand;
  }
}
?>
